defmodule BunyanTest do
  use ExUnit.Case

  test "it registers writers" do
    logger = Bunyan.new
    memory_1 = Memory.new
    memory_2 = Memory.new
    Bunyan.register(logger, Memory, memory_1)
    Bunyan.register(logger, Memory, memory_2)

    writers = Bunyan.writers(logger)
    assert Enum.member?(writers, memory_1)
    assert Enum.member?(writers, memory_2)
  end

  # test "it logs" do
  #   # {:ok, pid} = Memory.start
  #   # Bunyan.log(Memory, pid, %{some_value: "OTP!"})
  #   # assert Enum.member?(Memory.get(pid), %{some_value: "OTP!"})
  # end
end
