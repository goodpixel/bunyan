defmodule MemoryTest do
  use ExUnit.Case
  doctest Bunyan

  test "it adds to the log" do
    {:ok, pid} = Memory.new
    Memory.write(pid, %{some_value: "OTP!"})
    assert Enum.member?(Memory.get(pid), %{some_value: "OTP!"})
  end
end
