defmodule Memory do
  @behaviour Writer
  use GenServer

  # Client

  def new do
    GenServer.start_link(__MODULE__, [])
  end

  def write(pid, value) do
    GenServer.call(pid, {:write, value})
  end

  def get(pid) do
    GenServer.call(pid, :get)
  end

  # Server

  def handle_call({:write, %{} = value}, _from, state) do
    {:reply, value, List.insert_at(state, -1, value)}
  end

  def handle_call(:get, _from, state) do
    {:reply, state, state}
  end
end
