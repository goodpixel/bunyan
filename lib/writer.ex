defmodule Writer do
  @callback write(pid, map) :: String.t
end
